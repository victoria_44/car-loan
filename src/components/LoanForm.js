import React, { Component } from "react";
import FinanceTable from "./FinanceTable";

class LoanForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedYear: "1",
      price: 0,
      deliveryDate: new Date()

    };
    this.handleSubmit = this.handleSubmit.bind(this);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target
    this.setState({
      [target.name]: target.value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      price: this.state.price,
    })
  }

  calculateDeposit(price) {
    return (price / 100) * 15
  }

  render() {
    const { price, selectedYear, deliveryDate } = this.state

    return (
      <div>
        <form className="formStyle" onSubmit={this.handleSubmit}>
          <input
            type="number"
            name="price"
            placeholder="Vehicle Price"
            onChange={this.handleChange}
          />
          <p><input type="date" name="deliveryDate" onChange={this.handleChange} /></p>
          <label>
            1 Year
            <input
              type="radio"
              name="selectedYear"
              value="1"
              checked={this.state.selectedYear === "1"}
              onChange={this.handleChange}
            />
          </label>
          <label>
            2 Years
            <input
              type="radio"
              name="selectedYear"
              value="2"
              checked={this.state.selectedYear === "2"}
              onChange={this.handleChange}
            />
          </label>
          <label>
            3 Years
            <input
              type="radio"
              name="selectedYear"
              value="3"
              checked={this.state.selectedYear === "3"}
              onChange={this.handleChange}
            />
          </label>
          <p/><button type="submit">
            Search
          </button>
        </form>
        <FinanceTable price={price} selectedYear={selectedYear} deliveryDate={deliveryDate} />
      </div>
    );
  }
}

export default LoanForm;
