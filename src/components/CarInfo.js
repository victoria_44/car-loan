import React from "react"

function CarInfo(props) {
    return (
        <div className="box">
            <h3>
                {props.data.title.name}
            </h3>
            <h3>{props.data.branch.name}</h3>
            <h5>£{props.data.salesInfo.pricing.cashPrice} / £{props.data.salesInfo.pricing.monthlyPayment} monthly</h5>
            <img className="carPhoto" alt={props.data.branch.name} src={props.data.photos[0]}></img>
        </div>
    )
}

export default CarInfo