import React from "react";
import footerImage from "../assets/footer.png";

function Footer() {
  return(
  <footer>
  <img src={footerImage} alt="footer - contact numbers" />
  </footer>
  )
}

export default Footer;
