import React, { Component } from "react"
import moment from "moment"
import CarResults from './CarResults'

class FinanceTable extends Component {

    calculateDeposit(price) {
        return (price / 100) * 15
    }

    calculatePricePlan(price, selectedYear) {
        if (selectedYear === '3') {
            return price / 36
        } else if (selectedYear === '2') {
            return price / 24
        } else return price / 12

    }


    render() {
        const { price, selectedYear, deliveryDate } = this.props
        const amount = this.calculateDeposit(price)
        const date = moment(deliveryDate).format('MMMM Do YYYY')
        const pricePlan = this.calculatePricePlan(price, selectedYear)
        const startDate = moment(deliveryDate).add(1, 'month').format('MMMM Do YYYY')

        return (

            <div>
                <div className="spanStyle">
                    <h2>Deposit Amount: £{amount.toFixed(2)}</h2>
                    <h3>Year(s) Finance: {selectedYear}</h3>
                    <h3>Selected Date: {date} </h3>
                    <hr />
                    <h2>Price Plan</h2>
                    <h4> You would pay £{pricePlan.toFixed(2)} per month over {selectedYear} year(s)
                <br /> with Payments beginning on {startDate}

                        <br />With an initial payment of £{(pricePlan + 88).toFixed(2)}
                        <br />And a closing payment of £{(pricePlan + 20).toFixed(2)}
                    </h4>
                </div>
                <CarResults amount={amount} />
            </div>
        )
    }
}
export default FinanceTable