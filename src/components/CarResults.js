import React, { Component } from "react"
import CarInfo from './CarInfo'

class CarResults extends Component {

    state = {
        loading: true,
        cars: [],
    }

    async componentDidMount() {

        const url = "https://cors-anywhere.herokuapp.com/www.arnoldclark.com/used-cars/search.json?payment_type=monthly&max_price=81"
        const response = await fetch(url)

        const data = await response.json()
        this.setState({
            cars: data.searchResults,
            loading: false
        })
    }

    render() {
        return (
            <div className="box">
                {this.state.loading ? <h2>Finding your matches...</h2> : <span>
                    <h2>Cars in your price range:</h2>

                    {this.state.cars.map(function (car) {
                        return (
                            <CarInfo key={car.id} data={car} />
                        );
                    })}
                </span>}
            </div>
        )
    }
}

export default CarResults