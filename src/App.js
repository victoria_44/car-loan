import React, { Component } from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import LoanForm from "./components/LoanForm";


class App extends Component {


  render() {
    return (
      <div>
        <Header />
        <LoanForm />
        <Footer />
      </div>
    );
  }
}

export default App;
