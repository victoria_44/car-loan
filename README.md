### Loan Calculator
This tech test has been created to resemble the spec recieved from Arnold Clark.

## Technologies
* React
* JSX, JavaScript
* HTML/CSS

## To Run Project

1. `npm install`
2. `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser

## Next Steps
* Have the app hosted somewhere - Heroku or GitLab Pages
* **Testing** - very important and looking to grow skills with JEST 
* Make a more user-friendly UI - responsive design